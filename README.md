# Overview 
This package contains Linux driver code for a remote weather sensor received by a 433MHz wireless receiver connected to a gpio pin (rf433_waether). It also contains a dummy driver, which sends a message to the specified gpiopin (rf433_station_emulation).

# Build the driver as a dkms module #
   Just run the `install.sh` script. This copys the source to the appropriate location and notifys dkms of the new module. Dkms builds and installs the module with every kernelupdate.
   The kernelmodules can now be inserted with modprobe.

# Build the driver as a kernel module #
   Run `make install` in the `src/` folder, which builds and installs the module to `/lib/module/$(KERNELVERSION)/extra/rf433_weather.ko`. It can be loaded with `modprobe rf433_weather`. With every kernelupdate the module has be rebuild.
   Do the same for the rf433_station_emulation module, if needed.