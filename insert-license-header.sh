# LICENSE
#
# Linux rf433_weather device driver
# Copyright (C) 2015 Patrick Werneck <mail@patrick-werneck.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

PROJECT_ROOT="/root/rf433_weather_mod"

C_HEADER="/* LICENSE
*
* Linux rf433_weather device driver
* Copyright (C) 2015 Patrick Werneck <mail@patrick-werneck.de>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,\
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/"
SH_HEADER="# LICENSE
#
# Linux rf433_weather device driver
# Copyright (C) 2015 Patrick Werneck <mail@patrick-werneck.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

#insert in all c sources and headers
c_src=`find ${PROJECT_ROOT} -type f -regex '.*\.[hc]' -not -path '*.git*'`
for file in ${c_src}; do

   cat ${file} | grep '/* LICENSE' > /dev/null
   has_license=$?
   if [ ${has_license} -eq "1" ]; then
      cp ${file} /tmp/lincese_inserter.tmp
      echo -e "${C_HEADER}" > ${file}
      cat /tmp/lincese_inserter.tmp >> ${file}
   fi
done;

#insert in all Makefiles and bash scripts
sh_src=`find ${PROJECT_ROOT}  -type f -regex '.*\(\(\.sh\)\|\(Makefile\)\)' -not -path '*.git*'`
for file in ${sh_src}; do

   cat ${file} | grep '# LICENSE' > /dev/null
   has_license=$?
   if [ ${has_license} -eq "1" ]; then
      cp ${file} /tmp/lincese_inserter.tmp
      echo -e "${SH_HEADER}" > ${file}
      cat /tmp/lincese_inserter.tmp >> ${file}
   fi
done;















