# LICENSE
#
# Linux rf433_weather device driver
# Copyright (C) 2015 Patrick Werneck <mail@patrick-werneck.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

STATION=src/rf433_station_emulation/
WEATHER=src/rf433_weather/
SUBDIRS=$(STATION) $(WEATHER)

all: rf433_station_emulation rf433_weather

rf433_station_emulation:
	$(MAKE) -C $(STATION)

rf433_weather:
	$(MAKE) -C $(WEATHER)


.PHONY: clean $(SUBDIRS)

clean: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@ clean
