# LICENSE
#
# Linux rf433_weather device driver
# Copyright (C) 2015 Patrick Werneck <mail@patrick-werneck.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash
PACKAGE_NAME=rf433_weather
PACKAGE_VERSION=0.2

if ! hash dkms 2> /dev/null; then
   echo "need dkms for automatic module installation";
   exit 1;
fi

sudo dkms remove -m ${PACKAGE_NAME} -v ${PACKAGE_VERSION} --all
sudo cp -R . /usr/src/${PACKAGE_NAME}-${PACKAGE_VERSION}
#sudo rm -rf /usr/src/${PACKAGE_NAME}-${PACKAGE_VERSION}/.git/
sudo dkms add -m ${PACKAGE_NAME} -v ${PACKAGE_VERSION}
sudo dkms install -m ${PACKAGE_NAME} -v ${PACKAGE_VERSION}
