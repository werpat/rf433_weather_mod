/* LICENSE
 *
 * Linux rf433_weather device driver
 * Copyright (C) 2015 Patrick Werneck <mail@patrick-werneck.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/ktime.h>
#include <linux/hrtimer.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/workqueue.h>

#define DRIVER_LICENSE "GPL"
#define DRIVER_AUTHOR "Patrick Werneck <mail@patrick-werneck.de>"
#define DRIVER_DESCRIPTION "Emulate a rf receiver connected to configure pin, which receives pulse coded data"
#define DRIVER_NAME "rf433_station_emulation"
#ifndef DRIVER_VERSION
   #define DRIVER_VERSION "unkown"
#endif

#define DEFAULT_PIN 18

#define TICK(pin) {gpio_set_value(pin, 1);udelay(450);}
#define SYNC_SPACE(pin) {gpio_set_value(pin, 0);udelay(1000);udelay(1000);udelay(1000);udelay(1000);udelay(1000);udelay(1000);udelay(1000);udelay(1000);}
#define HIGH_SPACE(pin) {gpio_set_value(pin, 0);udelay(1000);udelay(1000);udelay(1000);udelay(1000);}
#define LOW_SPACE(pin) {gpio_set_value(pin, 0);udelay(1000);udelay(1000);}
#define SYNC_PULSE(pin) {TICK(pin); SYNC_SPACE(pin);}
#define LOW_PULSE(pin) {TICK(pin); LOW_SPACE(pin);}
#define HIGH_PULSE(pin) {TICK(pin); HIGH_SPACE(pin);}

MODULE_LICENSE(DRIVER_LICENSE);
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESCRIPTION);
MODULE_VERSION(DRIVER_VERSION);

static unsigned int secs = 10;
module_param(secs, uint, 0);
MODULE_PARM_DESC(secs, "Amount of seconds to sleep between each codesend");
static uint64_t msg;
static uint32_t msg_upper = 0x3ff;
static uint32_t msg_lower = 0xffffffff;
module_param(msg_upper, uint, 0);
MODULE_PARM_DESC(msg_upper, "upper 32bits of the msg");
module_param(msg_lower, uint, 0);
MODULE_PARM_DESC(Msg_lower, "lower 32bits of the msg");
static int pin = DEFAULT_PIN;
module_param(pin, int, 0);
MODULE_PARM_DESC(pin, "Output GPIO pin");

static int run = 1;

static int __init rf_weather_module_init(void);
static void __exit rf_weather_module_exit(void);

static void send(struct work_struct* work)
{
   unsigned int i = 0, v;

   while(run) {
      for(i = 0; i < 5; i++) {
         SYNC_PULSE(pin);
      }

      for(i = 0; i < 42; i++) {
         v = (msg >> (42 - 1 - i)) & 1;

         if(v) {
            HIGH_PULSE(pin);

         } else {
            LOW_PULSE(pin);
         }
      }

      SYNC_PULSE(pin);
      pr_info("send temp\n");

      ssleep(secs);
   }

}
DECLARE_WORK(w, send);
struct workqueue_struct* queue = NULL;
static int __init rf_weather_module_init(void)
{
   int retval;
   queue = create_singlethread_workqueue(DRIVER_NAME "#queue");
   msg = (((uint64_t) msg_upper) << 32) | msg_lower;

   if((retval = gpio_request_one(18, GPIOF_OUT_INIT_LOW, "RF_DATA"))) {
      pr_err(DRIVER_NAME " failed to set gpio % d to input with error % d\n", 18, retval);
      return retval;
   }

   if((retval = queue_work(queue, &w)) == 0) {
      pr_err(DRIVER_NAME " failed to queue work");
      return retval;
   }

   pr_info(DRIVER_NAME " loaded\n");
   return 0;
}

static void __exit rf_weather_module_exit(void)
{
   run = 0;
   flush_workqueue(queue);
   destroy_workqueue(queue);
   gpio_free(pin);
   pr_info(DRIVER_NAME " unloaded\n");
}

module_init(rf_weather_module_init);
module_exit(rf_weather_module_exit);

