/* LICENSE
 *
 * Linux rf433_weather device driver
 * Copyright (C) 2015 Patrick Werneck <mail@patrick-werneck.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <asm/atomic.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/ktime.h>
#include <linux/hashtable.h>
#include <linux/string.h>
#include "rf433_weather.h"

MODULE_LICENSE(DRIVER_LICENSE);
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESCRIPTION);
MODULE_VERSION(DRIVER_VERSION);

/*
 * type, struct and enum definitions
 */

struct station_device {
   atomic64_t data;

   struct timer_list remove_timer;
   struct hlist_node node;

   struct device* dev;
};

struct station_update_work {
   uint64_t data;
   struct work_struct work;
};

/*
 * function definitions
 */
static int __init rf_weather_module_init(void);
static void __exit rf_weather_module_exit(void);
static irqreturn_t gpio_isr(int irq, void* data);

static ssize_t station_count_show(struct device* dev, struct device_attribute* attr, char* buf);
static ssize_t pin_show(struct device* dev, struct device_attribute* attr, char* buf);
static ssize_t brand_show(struct device* dev, struct device_attribute* attr, char* buf);

static ssize_t temperature_show(struct device* dev, struct device_attribute* attr, char* buf);
static ssize_t humidity_show(struct device* dev, struct device_attribute* attr, char* buf);
static ssize_t battery_show(struct device* dev, struct device_attribute* attr, char* buf);
static ssize_t id_show(struct device* dev, struct device_attribute* attr, char* buf);
static ssize_t channel_show(struct device* dev, struct device_attribute* attr, char* buf);
static ssize_t ccode_show(struct device* dev, struct device_attribute* attr, char* buf);

static void update_device(struct work_struct* work);
static int register_station(struct station_device* sta, uint8_t station_id);
static void unregister_station(struct station_device* sta);
static void destroy_station(unsigned long ptr);

static inline int logic_value(unsigned long pulse);
static inline int is_valid_bitlen(unsigned int bitlen);
static inline int is_noise(unsigned long pulse, int value);
static inline int is_sync(unsigned long pulse);
static inline int is_data(unsigned long pulse);

/*
 * set init and exit function
 */
module_init(rf_weather_module_init);
module_exit(rf_weather_module_exit);

/*
 * module global variables
 */
static struct class s_weather_class = {
      .name = CLASS_NAME,
      .owner = THIS_MODULE
   };
struct class* weather_class = &s_weather_class;
EXPORT_SYMBOL(weather_class);

static DEVICE_ATTR_RO(temperature);
static DEVICE_ATTR_RO(humidity);
static DEVICE_ATTR_RO(battery);
static DEVICE_ATTR_RO(id);
static DEVICE_ATTR_RO(channel);
static DEVICE_ATTR_RO(ccode);

static struct attribute* station_attrs[] = {
   &dev_attr_temperature.attr,
   &dev_attr_humidity.attr,
   &dev_attr_battery.attr,
   &dev_attr_id.attr,
   &dev_attr_channel.attr,
   &dev_attr_ccode.attr,
   NULL
};
static const struct attribute_group station_attr_group = {
   .attrs = (struct attribute**) station_attrs
};

static DEVICE_ATTR_RO(pin);
static DEVICE_ATTR_RO(station_count);
static DEVICE_ATTR_RO(brand);
static struct attribute* gpio_rf433_attrs[] = {
   &dev_attr_pin.attr,
   &dev_attr_station_count.attr,
   &dev_attr_brand.attr,
   NULL
};
static const struct attribute_group gpio_rf433_attr_group = {
   .attrs = (struct attribute**) gpio_rf433_attrs
};

static struct device* gpio_rf433_device;
static atomic_t count = ATOMIC_INIT(0);
static int gpio_irq = -1;

static struct workqueue_struct* queue;
static struct station_update_work sta_work;

static DEFINE_HASHTABLE(hashtbl_stations, HASHTABLE_BIT_SIZE);

/*
 * module parameters
 * be sure that unsigned int is longer than 8 bits
 * otherwise pulse/space lengths are 255 maximum
 */

static int gpio_in_pin;
static int _gpio_in_pin = DEFAULT_PIN;
module_param_named(gpio_in_pin, _gpio_in_pin, int, S_IRUGO);
MODULE_PARM_DESC(gpio_in_pin, "GPIO pin connected to the data pin of the 433 MHz receiver module (default: 17)");

static int __init rf_weather_module_init(void)
{
   int retval;
   /*
    * Save and work with the configured pin, otherwise at module remove time eventually a different pin is released than requested
    */
   gpio_in_pin = _gpio_in_pin;

   hash_init(hashtbl_stations);

   if ((retval = class_register(weather_class))) {
      err("failed to register class "CLASS_NAME"\n");
      goto error0;
   }

   gpio_rf433_device = device_create(weather_class, NULL, MKDEV(0, 0), gpio_rf433_device, DEVICE_NAME);

   if (IS_ERR(gpio_rf433_device)) {
      retval = PTR_ERR(gpio_rf433_device);
      err("failed to create device " DEVICE_NAME " with error %d\n", retval);
      goto error1;
   }

   if ((retval = sysfs_create_group(&gpio_rf433_device->kobj, &gpio_rf433_attr_group))) {
      err("failed to create sysfs files for device " DEVICE_NAME " with error %d\n", retval);
      goto error2;
   }

   if (IS_ERR((queue = create_singlethread_workqueue("rf433_weather#queue")))) {
      retval = PTR_ERR(queue);
      err("failed to created queue with error %d\n", retval);
      goto error2;
   }

   if ((retval = gpio_request_one(gpio_in_pin, GPIOF_IN, "RF_DATA"))) {
      err("failed to set gpio%d to input with error %d\n", gpio_in_pin, retval);
      goto error3;
   }

   if ((gpio_irq = gpio_to_irq(gpio_in_pin)) < 0) {
      err("failed to get irq number of gpio%d with error %d\n", gpio_in_pin, gpio_irq);
      retval = gpio_irq;
      goto error4;
   }

   if ((retval = request_irq(gpio_irq, gpio_isr, IRQF_TRIGGER_FALLING | IRQF_TRIGGER_RISING, "rf_weather#RF_DATA", NULL))) {
      err("failed to request irq %d\n", retval);
      goto error4;
   }

   info("loaded, gpio_in_pin=%d\n", gpio_in_pin);

   return RETURN_SUCCESS;

error4:
   gpio_free(gpio_in_pin);
error3:
   flush_workqueue(queue);
   destroy_workqueue(queue);
error2:
   device_unregister(gpio_rf433_device);
error1:
   class_unregister(weather_class);
error0:
   return retval;
}

static void __exit rf_weather_module_exit(void)
{
   int bkt;
   struct station_device* sta = NULL;

   //stop listening on gpio port
   free_irq(gpio_irq, NULL);
   gpio_free(gpio_in_pin);

   //remove scheduled tasks and destroy workqueue
   flush_workqueue(queue);
   destroy_workqueue(queue);

   //unregister all device attributes, delete remove_timers, free all allocated memory
   hash_for_each(hashtbl_stations, bkt, sta, node) {
      destroy_station((unsigned long) sta);
   }
   //remove device
   device_unregister(gpio_rf433_device);
   //unregister class
   class_unregister(weather_class);
   info("unloaded\n");
}

static int register_station(struct station_device* sta, uint8_t station_id)
{
   int retval = 0;

   if (sta) {
      //register device
      memset(sta, 0, sizeof(struct station_device));
      sta->dev = device_create(weather_class, gpio_rf433_device, MKDEV(0, 0), sta, "station_%d", station_id);

      if (IS_ERR(sta->dev)) {
         retval = PTR_ERR(sta->dev);
         err("failed to register station device for station %d with error %d\n", station_id, retval);
         goto error0;
      }

      if ((retval = sysfs_create_group(&sta->dev->kobj, &station_attr_group))) {
         err("failed to create sysfs files for station %d with error %d\n", station_id, retval);
         goto error1;
      }

      //init and start remove timer
      init_timer(&sta->remove_timer);
      sta->remove_timer.data = (unsigned long) sta;
      sta->remove_timer.function = &destroy_station;
      sta->remove_timer.expires = jiffies + STATION_REMOVE_TIMEOUT * HZ;
      add_timer(&sta->remove_timer);

      //add to hash_table
      hash_add(hashtbl_stations, &sta->node, station_id);

      atomic_inc(&count);

      info("register station with id %d\n", station_id);
      return RETURN_SUCCESS;

   } else {
      return -EINVAL;
   }

error1:
   device_unregister(sta->dev);
error0:
   return retval;
}

static void destroy_station(unsigned long ptr)
{
   struct station_device* sta = (struct station_device*) ptr;
   unregister_station(sta);
   kfree(sta);
}

static void unregister_station(struct station_device* sta)
{
   uint8_t station_id;

   if (sta) {
      station_id = (uint8_t) STATION_ID(atomic64_read(&sta->data));
      info("unregister station with id %d\n", station_id);

      //remove from hashtable
      hash_del(&sta->node);

      //delete remove_timer
      del_timer_sync(&sta->remove_timer);

      atomic_dec(&count);

      //unregister device
      device_unregister(sta->dev);
   }
}

static void update_device(struct work_struct* work)
{
   struct station_update_work* sta_work = container_of(work, struct station_update_work, work);
   uint64_t data = sta_work->data;
   uint8_t station_id = (uint8_t) STATION_ID(data);
   struct station_device* sta = NULL;
   struct station_device* sta_iter = NULL;
   uint8_t id_iter;
   int status;
   //seach for existing station in hash table
   hash_for_each_possible(hashtbl_stations, sta_iter, node, station_id) {
      id_iter = (uint8_t) STATION_ID(atomic64_read(&sta_iter->data));

      if (station_id == id_iter) {
         sta = sta_iter;
         break;
      }
   }

   if (sta == NULL) {
      sta = (struct station_device*) kmalloc(sizeof(struct station_device), GFP_KERNEL);

      if ((status = register_station(sta, station_id))) {
         //TODO: errorhandling
         err("failed to register new station [id %d] with error %d\n", station_id, status);
         return;
      }

   } else {
      //update remove timer
      mod_timer(&sta->remove_timer, jiffies + STATION_REMOVE_TIMEOUT * HZ);
   }

   //update data
   atomic64_set(&sta->data, data);

   info("update device with msg=%llX\n", sta_work->data);
}

static irqreturn_t gpio_isr(int irq, void* data)
{
   static int synced = 0, data_ctr;
   static ktime_t last = {};
   static uint64_t sequence;
   ktime_t now = ktime_get();
   uint32_t pulse = (uint32_t) ktime_us_delta(now, last);
   int value = gpio_get_value(gpio_in_pin);

   last = now;

   if (is_noise(pulse, value)) {
      synced = sequence = data_ctr = 0;

   } else if (value && is_sync(pulse)) {
      sequence = data_ctr = 0;
      synced = 1;

   } else if (value && synced && is_data(pulse)) {
      sequence <<= 1;
      sequence |= logic_value(pulse);
      data_ctr++;

      if (is_valid_bitlen(data_ctr)) {
         INIT_WORK(&sta_work.work, update_device);
         sta_work.data = sequence;
         queue_work(queue, &sta_work.work);

      }

   } else if (value) {
      data_ctr = synced = 0;
   }

   return IRQ_HANDLED;
}

static inline int is_valid_bitlen(unsigned int bitlen)
{
   return bitlen >= STATION_DATA_PULSES;
}

static inline int is_noise(unsigned long pulse, int value)
{
   return (value && (pulse < STATION_MIN_LOW || pulse > STATION_MAX_LOW)) || ((!value) && (pulse < STATION_MIN_HIGH || pulse > STATION_MAX_HIGH));
}

static inline int is_sync(unsigned long pulse)
{
   return pulse > STATION_MIN_SYNC_LOW && pulse < STATION_MAX_SYNC_LOW;
}

static inline int is_data(unsigned long pulse)
{
   return pulse > STATION_MIN_DATA_LOW && pulse < STATION_MAX_DATA_LOW;
}

static inline int logic_value(unsigned long pulse)
{
   return pulse > STATION_LOGIC_SEPERATOR;
}

static ssize_t station_count_show(struct device* dev, struct device_attribute* attr, char* buf)
{
   ssize_t status;
   status = snprintf(buf, PAGE_SIZE, "%d\n", atomic_read(&count));
   return status;
}
static ssize_t pin_show(struct device* dev, struct device_attribute* attr, char* buf)
{
   ssize_t status;
   status = snprintf(buf, PAGE_SIZE, "%d\n", gpio_in_pin);
   return status;
}
static ssize_t brand_show(struct device* dev, struct device_attribute* attr, char* buf)
{
   ssize_t status = 0;
   status = snprintf(buf, PAGE_SIZE, "%s\n", STATION_BRAND);

   return status;
}

static ssize_t temperature_show(struct device* dev, struct device_attribute* attr, char* buf)
{
   ssize_t status;
   struct station_device* sta = (struct station_device*) dev_get_drvdata(dev);
   uint64_t data = atomic64_read(&sta->data);
   status = snprintf(buf, PAGE_SIZE, "%hi\n", STATION_TEMPERATURE(data));
   return status;
}
static ssize_t humidity_show(struct device* dev, struct device_attribute* attr, char* buf)
{
   ssize_t status;
   struct station_device* sta = (struct station_device*) dev_get_drvdata(dev);
   uint64_t data = atomic64_read(&sta->data);
   status = snprintf(buf, PAGE_SIZE, "%hhu\n", (uint8_t) STATION_HUMIDITY(data));
   return status;
}
static ssize_t battery_show(struct device* dev, struct device_attribute* attr, char* buf)
{
   ssize_t status;
   struct station_device* sta = (struct station_device*) dev_get_drvdata(dev);
   uint64_t data = atomic64_read(&sta->data);
   status = snprintf(buf, PAGE_SIZE, "%hhu\n", (uint8_t) STATION_BATTERY(data));
   return status;
}
static ssize_t id_show(struct device* dev, struct device_attribute* attr, char* buf)
{
   ssize_t status;
   struct station_device* sta = (struct station_device*) dev_get_drvdata(dev);
   uint64_t data = atomic64_read(&sta->data);
   status = snprintf(buf, PAGE_SIZE, "%hhu\n", (uint8_t) STATION_ID(data));
   return status;
}
static ssize_t channel_show(struct device* dev, struct device_attribute* attr, char* buf)
{
   ssize_t status;
   struct station_device* sta = (struct station_device*) dev_get_drvdata(dev);
   uint64_t data = atomic64_read(&sta->data);
   status = snprintf(buf, PAGE_SIZE, "%hhu\n", (uint8_t) STATION_CHANNEL(data));
   return status;
}
static ssize_t ccode_show(struct device* dev, struct device_attribute* attr, char* buf)
{
   ssize_t status;
   struct station_device* sta = (struct station_device*) dev_get_drvdata(dev);
   uint64_t data = atomic64_read(&sta->data);
   status = snprintf(buf, PAGE_SIZE, "%hhu\n", (uint8_t) STATION_CCODE(data));
   return status;
}

