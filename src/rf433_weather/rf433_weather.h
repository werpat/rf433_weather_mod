/* LICENSE
 *
 * Linux rf433_weather device driver
 * Copyright (C) 2015 Patrick Werneck <mail@patrick-werneck.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __WEATHER_MOD_H
#define __WEATHER_MOD_H

#define RETURN_SUCCESS 0
#define RETURN_GENERIC_FAIL -1

#define DRIVER_LICENSE "GPL"
#define DRIVER_AUTHOR "Patrick Werneck <mail@patrick-werneck.de>"
#define DRIVER_DESCRIPTION "A module which reads temperature data over a connected 433 mhz receiver"
#ifndef DRIVER_VERSION
   #define DRIVER_VERSION "unkown"
#endif /* DRIVER_VERSION */

#define CLASS_NAME "weather"
#define DRIVER_NAME "rf433_weather"
#define DEVICE_NAME "gpio_rf433_" CLASS_NAME
#define STATION_REMOVE_TIMEOUT 15*60
#define DEFAULT_PIN 17

#define HASHTABLE_BIT_SIZE 4

#define info(format, arg...) pr_info(DRIVER_NAME ": " format, ## arg)
#define warn(format, arg...) pr_warn(DRIVER_NAME ": " format, ## arg)
#define err(format, arg...) pr_err(DRIVER_NAME ": " format, ## arg)


#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

#define STATION_BRAND "Tchibo Wetterstation <XYZ>"
#define STATION_MIN_HIGH 350
#define STATION_MAX_HIGH 550
#define STATION_MIN_DATA_LOW 1600
#define STATION_MAX_DATA_LOW 4600
#define STATION_LOGIC_SEPERATOR ((STATION_MIN_DATA_LOW + STATION_MAX_DATA_LOW) / 2)

#ifdef STATION_STRICT
   #define STATION_MIN_SYNC_LOW 7850
   #define STATION_MAX_SYNC_LOW 8150
#else
   #define STATION_MIN_SYNC_LOW 7500
   #define STATION_MAX_SYNC_LOW 8500
#endif /* STATION_CONSERVATIVE */

#if STATION_MIN_DATA_LOW < STATION_MIN_SYNC_LOW
   #define STATION_MIN_LOW STATION_MIN_DATA_LOW
#else
   #define STATION_MIN_LOW STATION_MIN_SYNC_LOW
#endif /* STATION_MIN_DATA_LOW < STATION_MIN_SYNC_LOW */

#if STATION_MAX_DATA_LOW > STATION_MAX_SYNC_LOW
   #define STATION_MAX_LOW STATION_MAX_DATA_LOW
#else
   #define STATION_MAX_LOW STATION_MAX_SYNC_LOW
#endif /* STATION_MAX_DATA_LOW > STATION_MAX_SYNC_LOW */

#define STATION_DATA_PULSES 42

#define STATION_ID(data) (((data) >> 32) & 0xff)
#define STATION_TEMPERATURE_F(data) ((int32_t)(((((data) >> 24) & 0xf) \
      | (((data) >> 16) & 0xf0) \
      | (((data) >> 8) & 0xf00))) - 900)
#define STATION_TEMPERATURE_C(data) ((STATION_TEMPERATURE_F(data) - 320) * 500 / 9)

#ifdef UNIT_FAHRENHEIT
   #define STATION_TEMPERATURE(data) (STATION_TEMPERATURE_F(data) * 10)
#else
   #define STATION_TEMPERATURE(data) (STATION_TEMPERATURE_C(data))
#endif /* UNIT_FAHRENHEIT */

#define STATION_HUMIDITY(data) ((((data) >> 12) & 0xf) | (((data) >> 4) & 0xf0))
#define STATION_BATTERY(data) (((data) >> 30) & 0x3)
#define STATION_CHANNEL(data) (((data) >> 28) & 0x3)
#define STATION_CCODE(data) ((data) & 0xff)

#endif /*__WEATHER_MOD_H*/

